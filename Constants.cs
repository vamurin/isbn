﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISBN
{
    public static class Constants
    {
        public const int NineDigitSbnLength = 9;
        public const int TenDigitIsbnLength = 10;
        public const int ThirteenDigitIsbnLength = 13;
        public const int IsbnStartWith978 = 978;
        public const int IsbnStartWith979 = 979;
    }
}
