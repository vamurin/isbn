﻿using System;

namespace ISBN
{
    public class Program
    {
        static void Main()
        {
            var strISBN = "2-266-11156-6";

            Console.WriteLine($"ISBN = {strISBN}");
            Console.WriteLine($"ISBN is {ISBN.IsISBN(strISBN)}");

            strISBN = "978-0-306-40615-7";

            Console.WriteLine($"ISBN = {strISBN}");
            Console.WriteLine($"ISBN is {ISBN.IsISBN(strISBN)}");
            Console.WriteLine("");

            var gs1 = 978;
            var group = 0;
            var registrant = 306;
            var publication = 40615;

            Console.WriteLine($"13-digit ISBN = {ISBN.CreateISBN(gs1, group, registrant, publication)}");

            group = 2;
            registrant = 266;
            publication = 11156;
            Console.WriteLine($"10-digit ISBN = {ISBN.CreateISBN(group, registrant, publication)}");

            Console.Read();
        }

       
    }

    
}
