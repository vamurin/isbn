﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISBN
{
    public static class ISBN
    {
        private static int GetCheckDigitForISBN(string isbn)
        {
            var sum = 0;
            var i = 0;
            if (isbn.Length == Constants.TenDigitIsbnLength || isbn.Length == Constants.ThirteenDigitIsbnLength)
                isbn = isbn.Remove(isbn.Length - 1);
            var result = -1;
            switch (isbn.Length)
            {
                case Constants.TenDigitIsbnLength - 1:
                    foreach (var ch in isbn)
                    {
                        sum += (10 - i) * (int)char.GetNumericValue(ch);
                        i++;
                    }

                    result = (11 - (sum % 11)) % 11;

                    return result;
                case Constants.ThirteenDigitIsbnLength - 1:
                    foreach (var ch in isbn)
                    {
                        sum += ((i % 2 == 0) ? 1 : 3) * (int)char.GetNumericValue(ch);
                        i++;
                    }
                    result = (10 - (sum % 10));
                    if (result == 10) result = 0;

                    return result;
                default:
                    return result;
            }

        }
        
        public static bool IsISBN(string str)
        {
            // remove all hyphens as ISBN groups can be separated by hyphens
            var isbn = str.Replace("-", string.Empty);

            // if ISBN is 9 digit, it might be SBN and can be converted to ISBN by adding 0 to the beginning
            if (isbn.Length == Constants.NineDigitSbnLength) isbn = $"0{isbn}";

            // ISBN is either 10 digits or 13 digits
            if (!(isbn.Length == Constants.TenDigitIsbnLength || 
                isbn.Length == Constants.ThirteenDigitIsbnLength)) return false;

            // the first 3 digits of 13 digit ISBN must be either 978 or 979
            if (isbn.Length == Constants.ThirteenDigitIsbnLength && 
                !(isbn.StartsWith(Constants.IsbnStartWith978.ToString()) || 
                  isbn.StartsWith(Constants.IsbnStartWith979.ToString())))
                return false;


            if (isbn.Length == Constants.TenDigitIsbnLength)
            {
                // 10-digit ISBN contains numbers only (except for the last digit that can also take 'x')
                var lc = isbn.Last();
                var isbnWithoutLast = isbn.Remove(isbn.Length - 1);
                if (isbnWithoutLast.Any(x => !char.IsDigit(x)) || !(char.IsDigit(lc) || lc == 'x')) return false;

                // last digit checksum for 10-digit ISBN is valid
                var checkDigit = GetCheckDigitForISBN(isbn);
                if (checkDigit < 10 && checkDigit != (int)char.GetNumericValue(lc))
                    return false;
                if (checkDigit == 10 && lc != 'x')
                    return false;
            }


            if (isbn.Length == Constants.ThirteenDigitIsbnLength)
            {
                // 13-digit ISBN contains numbers only
                if (isbn.Any(x => !char.IsDigit(x))) return false;

                // last digit checksum for 13-digit ISBN is valid
                var checkDigit = GetCheckDigitForISBN(isbn);
                if (checkDigit != (int)char.GetNumericValue(isbn.Last()))
                    return false;
            }

            return true;
        }

        public static string CreateISBN(int gs1, int group, int registrant, int publication)
        {
            // for 13-digit ISBN starting prefix should be 978 or 979
            if (!(gs1 != Constants.IsbnStartWith978 || gs1 != Constants.IsbnStartWith979)) return "";

            var isbn = gs1.ToString() + group + registrant + publication;

            // the resulting ISBN string shall be 12 digits (without the last check digit)
            if (isbn.Length != Constants.ThirteenDigitIsbnLength - 1) return "";

            var checkDigit = GetCheckDigitForISBN(isbn);

            // construct the final string with hyphens
            isbn = $"{gs1.ToString()}-{group.ToString()}-{registrant.ToString()}-{publication.ToString()}-{checkDigit.ToString()}";

            return isbn;
        }

        public static string CreateISBN(int group, int registrant, int publication)
        {
            var isbn = group.ToString() + registrant + publication;

            // the resulting ISBN string shall be 9 digits (without the last check digit)
            if (isbn.Length != Constants.TenDigitIsbnLength - 1) return "";

            var checkDigit = GetCheckDigitForISBN(isbn);

            // construct the final string with hyphens
            isbn = $"{group.ToString()}-{registrant.ToString()}-{publication.ToString()}-{checkDigit.ToString()}";

            return isbn;
        }
    }
}
